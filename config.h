#include <X11/XF86keysym.h>
#include "zoomswap.c"
#include "movestack.c"
#include "fibonacci.c"

/* appearance */
static const unsigned int borderpx       = 0;        /* border pixel of windows */
static const unsigned int snap           = 32;       /* snap pixel */
static const unsigned int gappx          = 20;
static const int showbar                 = 1;        /* 0 means no bar */
static const int topbar                  = 1;        /* 0 means bottom bar */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;     /* 0 means no systray */
static const char *fonts[]               = { "Hack:size=11", "FontAwesome:size=14" };
static const char col_bg[]               = "#1b1918";
static const char col_fg[]               = "#f1efee";
static const char col_grey[]             = "#a8a19f";
static const char col_blue[]             = "#407ee7";
static const char *colors[][3]           = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_grey,  col_bg,    col_bg },
	[SchemeSel]  = { col_blue,  col_bg,    col_bg  },
};

/* tagging */
static const char *tags[] = { "MAIN", "WEB", "CODE", "LERN", "COMM", "MUSE", "PAD" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class           instance    title       tags mask     isfloating   monitor */
	{ "Slack",         NULL,       NULL,       1 << 4,       0,           -1 },
	{ "Discord",       NULL,       NULL,       1 << 4,       0,           -1 },
	{ "Newsboat",      NULL,       NULL,       1 << 4,       0,           -1 },
	{ "Popcorn-Time",  NULL,       NULL,       1 << 5,       0,           -1 },
	{ "Cmus",          NULL,       NULL,       1 << 5,       0,           -1 },
	{ "Spotify",       NULL,			 NULL,       1 << 5,       0,           -1 },
	{ "Alacritty",		 "Note",     "Note",     TAGMASK,      1,           -1 },
	{ "Alacritty",		 "Cod",      "Cod",			 TAGMASK,      1,           -1 },
	{ "Alacritty",		 "Calendar", "Calendar", TAGMASK,      1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
  /* symbol  arrange function */
  {  "[T]",    tile },    /* first entry is default */
  {  "[F]",    NULL },    /* no layout function means floating behavior */
  {  "[M]",    monocle }, /* monocle is good for maximizing the preservation and focusing of the window */
	{  "[@]",    spiral },
	{  "[\]",    dwindle },
};

// statusbutton
static const char buttonbar[] = " ";

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]                = { "rofi", "-show", "drun", NULL };
static const char *termcmd[]                 = { "alacritty", NULL };
static const char *filemanagercmd[]          = { "alacritty", "-t", "nnn",  "-e", "zsh", "-lc", "nnn -cdEFrx", NULL };
static const char *notescmd[]                = { "alacritty", "--class", "Note", "-t", "Note", "-e", "zsh", "-lc", "note", NULL };
static const char *todocmd[]								 = { "alacritty", "--hold", "--class", "Cod", "-t", "Cod", "-e", "zsh", "-lc", "cod l", NULL };
static const char *calendarcmd[]						 = { "alacritty", "--hold", "--class", "Calendar", "-t", "Calendar", "-e", "zsh", "-lc", "calendar", NULL };
static const char *volumemutecmd[]           = { "volumectrl", "mute", NULL };
static const char *volumedowncmd[]           = { "volumectrl", "down",  NULL };
static const char *volumeupcmd[]             = { "volumectrl", "up",  NULL };
static const char *micmutecmd[]              = { "volumectrl", "mic", NULL };
static const char *brightnessdowncmd[]       = { "xbacklight", "-dec", "10", NULL };
static const char *brightnessupcmd[]         = { "xbacklight", "-inc", "10", NULL };
static const char *displaycmd[]              = { "arandr", NULL };
static const char *browsercmd[]              = { "firefox", NULL };
static const char *musicpausetogglecmd[]     = { "playerctl", "play-pause", NULL };
static const char *musicplaypreviouscmd[]    = { "playerctl", "previous", NULL };
static const char *musicplaynextcmd[]        = { "playerctl", "next", NULL };
static const char *switchkeyboardlayoutcmd[] = { "switch-layout", NULL };
static const char *screenlockcmd[]           = { "sysmenu", "lock", NULL };
static const char *suspendncmd[]             = { "sysmenu", "suspend", NULL };
static const char *rebootcmd[]               = { "sysmenu", "reboot", NULL };
static const char *shutdowncmd[]             = { "sysmenu", "shutdown", NULL };
static const char *printallscreencmd[]       = { "printscreen", "all", NULL };
static const char *printfocusedscreencmd[]   = { "printscreen", "focused", NULL };

static Key keys[] = {
	/* modifier               key                         function        argument */
	{ MODKEY,                 XK_p,                       spawn,          {.v = dmenucmd } },
	{ MODKEY,                 XK_t,                       spawn,          {.v = termcmd } },
	{ MODKEY,                 XK_f,                       spawn,          {.v = filemanagercmd } },
	{ MODKEY,                 XK_n,                       spawn,          {.v = notescmd } },
	{ MODKEY,                 XK_l,                       spawn,          {.v = todocmd } },
	{ MODKEY,                 XK_c,                       spawn,          {.v = calendarcmd } },
  { 0,                      XF86XK_AudioMute,           spawn,          {.v = volumemutecmd } },
  { 0,                      XF86XK_AudioLowerVolume,    spawn,          {.v = volumedowncmd } },
  { 0,                      XF86XK_AudioRaiseVolume,    spawn,          {.v = volumeupcmd } },
  { 0,                      XF86XK_AudioMicMute,        spawn,          {.v = micmutecmd } },
  { 0,                      XF86XK_MonBrightnessDown,   spawn,          {.v = brightnessdowncmd } },
	{ 0,                      XF86XK_MonBrightnessUp,     spawn,          {.v = brightnessupcmd } },
	{ 0,                      XF86XK_Display,             spawn,          {.v = displaycmd } },
	{ 0,											XK_F2,                      spawn,          {.v = brightnessdowncmd } },
	{ 0,											XK_F3,                      spawn,          {.v = brightnessupcmd } },
	{ 0,                      XK_F4,											spawn,          {.v = displaycmd } },
  { 0,                      XK_F6,											spawn,          {.v = micmutecmd } },
	{ 0,                      XK_Print,                   spawn,          {.v = printallscreencmd } },
	{ MODKEY,                 XK_Print,                   spawn,          {.v = printfocusedscreencmd } },
	{ MODKEY,                 XK_b,                       spawn,          {.v = browsercmd } },
	{ MODKEY|ShiftMask,       XK_p,                       spawn,          {.v = musicpausetogglecmd } },
	{ MODKEY|ShiftMask,       XK_bracketleft,             spawn,          {.v = musicplaypreviouscmd } },
	{ MODKEY|ShiftMask,       XK_bracketright,            spawn,          {.v = musicplaynextcmd } },
	{ MODKEY,                 XK_space,                   spawn,          {.v = switchkeyboardlayoutcmd } },
	{ MODKEY|ShiftMask,       XK_b,						            togglebar,			{0} },
	{ MODKEY,                 XK_j,                       focusstack,     {.i = +1 } },
	{ MODKEY,                 XK_k,                       focusstack,     {.i = -1 } },
	{ MODKEY,                 XK_i,                       incnmaster,     {.i = +1 } },
	{ MODKEY,                 XK_d,                       incnmaster,     {.i = -1 } },
	{ MODKEY,                 XK_h,                       setmfact,       {.f = -0.05} },
	{ MODKEY,                 XK_l,                       setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,       XK_j,                       movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,       XK_k,                       movestack,      {.i = -1 } },
	{ MODKEY,                 XK_Return,                  zoom,           {0} },
	{ MODKEY,                 XK_Tab,                     view,           {0} },
	{ MODKEY,                 XK_q,                       killclient,     {0} },
	{ MODKEY|ShiftMask,       XK_t,                       setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,       XK_f,                       setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,       XK_m,                       setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,       XK_g,												setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,       XK_space,                   togglefloating, {0} },
	{ MODKEY,                 XK_0,                       view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,       XK_0,                       tag,            {.ui = ~0 } },
	{ MODKEY,                 XK_comma,                   focusmon,       {.i = -1 } },
	{ MODKEY,                 XK_period,                  focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,       XK_comma,                   tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,       XK_period,                  tagmon,         {.i = +1 } },
	TAGKEYS(                  XK_1,                                       0)
	TAGKEYS(                  XK_2,                                       1)
	TAGKEYS(                  XK_3,                                       2)
	TAGKEYS(                  XK_4,                                       3)
	TAGKEYS(                  XK_5,                                       4)
	TAGKEYS(                  XK_6,                                       5)
	TAGKEYS(                  XK_7,                                       6)
	TAGKEYS(                  XK_8,                                       7)
	TAGKEYS(                  XK_9,                                       8)
  { MODKEY|ShiftMask,       XK_l,                       spawn,          {.v = screenlockcmd } },
  { MODKEY|ShiftMask,       XK_s,                       spawn,          {.v = suspendncmd } },
  { MODKEY|ShiftMask,       XK_r,                       spawn,          {.v = rebootcmd } },
  { MODKEY|ShiftMask,       XK_d,                       spawn,          {.v = shutdowncmd } },
	{ MODKEY|ShiftMask,       XK_q,                       quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkButton,						0,							Button1,			  spawn,					{.v = dmenucmd } },
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
